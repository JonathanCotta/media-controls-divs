//requires
var g = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-csso');
var rename = require('gulp-rename');
var brw = require('browser-sync').create();

//default
g.task('default',['srv', 'sass-sync', 'index-sync']);

//watchers
g.task('sass-sync', function() {
  return g.watch('./assets/sass/*.scss',['sass','srv-sync']);
});

g.task('index-sync', function() {
  return g.watch('./index.html',['srv-sync']);
});

//tasks
g.task("sass",function() {
  return g.src('./assets/sass/main.scss')
  .pipe(sass())
  .pipe(minify())
  .pipe(rename('style.min.css'))
  .pipe(g.dest('./assets/css/'));
});

//browser-sync
g.task('srv', function() {
  return brw.init({
    server:'./',
    port: 6060,
    ui:{port:7070}
  })
});

g.task('srv-sync', function() {
  return brw.reload();
});
